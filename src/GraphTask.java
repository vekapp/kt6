import java.util.ArrayList;
import java.util.List;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */
    public void run() {
        Graph h = new Graph("Asymptomia");

        Vertex f = h.createVertex("F");
        Vertex g = h.createVertex("G");
        Vertex e = h.createVertex("E");
        Vertex d = h.createVertex("D");
        Vertex c = h.createVertex("C");
        Vertex b = h.createVertex("B");
        Vertex a = h.createVertex("A");
        h.setEnter(a);
        h.setExit(f);
        Arc ab = h.createArc("A->B", a, b, 11);
        h.createArc("A->D", a, d, 5);
        Arc bd = h.createArc("B->D", b, d, 1);
        h.createArc("D->E", d, e, 3);
        Arc ec = h.createArc("E->C", e, c, 5);
        h.createArc("C->B", c, b, 7);
        h.createArc("E->F", e, f, 21);
        Arc cf = h.createArc("C->F", c, f, 10);
        Arc dc = h.createArc("D->C", d, c, 8);
        h.createArc("A->G", a, g, 4);
        h.createArc("E->G", e, g, 7);
        h.createArc("G->D", g, d, 6);
        ab.isRedLine = true;
        bd.isRedLine = true;
        ec.isRedLine = true;
        cf.isRedLine = true;
        dc.isRedLine = true;

        /*
        Graph h = new Graph("H");
        Vertex ten = h.createVertex("10");
        Vertex nine = h.createVertex("9");
        Vertex eight = h.createVertex("8");
        Vertex seven = h.createVertex("7");
        Vertex six = h.createVertex("6");
        Vertex five = h.createVertex("5");
        Vertex four = h.createVertex("4");
        Vertex three = h.createVertex("3");
        Vertex two = h.createVertex("2");
        Vertex one = h.createVertex("1");
        h.setEnter(one);
        h.setExit(ten);
        Arc nineTen = h.createArc("9-10", nine, ten, 1);
        Arc eightTen = h.createArc("8-10", eight, ten, 1);
        Arc sevenNine = h.createArc("7-9", seven, nine, 2);
        Arc sixNine = h.createArc("6-9", six, nine, 3);
        Arc fiveEight = h.createArc("5-8", five, eight, 4);
        Arc fourEight = h.createArc("4-8", four, eight, 2);
        Arc fourSeven = h.createArc("4-7", four, seven, 4);
        Arc threeSeven = h.createArc("3-7", three, seven, 1);
        Arc twoSix = h.createArc("2-6", two, six, 3);
        Arc twoThree = h.createArc("2-3", two, three, 5);
        Arc oneFive = h.createArc("1-5", one, five, 4);
        Arc oneFour = h.createArc("1-4", one, four, 2);
        Arc oneThree = h.createArc("1-3", one, three, 6);
        Arc oneTwo = h.createArc("1-2", one, two,1);
        oneTwo.isRedLine = true;
        twoThree.isRedLine = true;
        twoSix.isRedLine = true;
        fourEight.isRedLine = true;
        fourSeven.isRedLine = true;
        sixNine.isRedLine = true;
        eightTen.isRedLine = true;
        h.createRandomDirectedGraph(30, 60);
         */

        System.out.println("------------------------- Veiko oma -----------------------------");
        System.out.println(h);
        System.out.println(h.getProfitPath());
        System.out.println("The most profitable path for the Knight through the Castle of Asymptopia is : " + h.pathToString());
        System.out.println();
        System.out.println("------------------------- Siimu oma -----------------------------");
        System.out.println();
        h.getAllRoutes(h.first, h.exit, new ArrayList<>());
        System.out.println(h.getClientRoutes(0));
        System.out.println(h.getClientRoutes(2));
        System.out.println(h.getClientRoutes(3));
        System.out.println(h.getClientRoutes(4));

        System.out.println();
        System.out.println("------------------------- Jaanis oma -----------------------------");
        System.out.println();

        System.out.println("Original graph: " + h);
        System.out.println("Line graph: " + h.convertToLineGraph());
        System.out.println();
        System.out.println("------------------------- Kristjani oma -----------------------------");
        System.out.println();
        System.out.println(h.graphToThePowerOf2().toString());
        System.out.println();
        System.out.println("------------------------- Otto oma -----------------------------");
        System.out.println();
        System.out.println(h);
        h.deleteVertexAndItsPredecessors(h.exit);
        System.out.println(h);
    }

    /**
     * Vertex represents one point in the graph.
     */
    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;
        private int vertexInfo = 0;
        private boolean enter;
        private boolean exit;

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
            enter = false;
            exit = false;
        }

        public void setEnter(boolean enter) {
            this.enter = enter;
        }

        public void setExit(boolean exit) {
            this.exit = exit;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            if (enter) {
                return "(Start) " + id;
            } else if (exit) {
                return "(Exit)  " + id;
            } else {
                return "        " + id;
            }
        }

        /** Set the info value for the Vertex.*/
        public void setVInfo(int i) {
            vertexInfo = i;
        }

        /** Return the id value of the Vertex.*/
        public String getVertexId() {
            return id;
        }

        /** Return the info value of the Vertex.*/
        public int getCustomInfo() {
            return vertexInfo;
        }
    }


    /**
     * Arc represents one arrow in the graph.
     */
    class Arc {

        private String id;
        private Vertex target;
        private Vertex from;
        private Arc next;
        private int info = 0;
        private int value;
        private boolean isRedLine;

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return from.id + "-(" + value + ")->" + target.id;
        }

        /** Set the info value for the Arc.*/
        public void setInfo(int i) {
            info = i;
        }

        /** Return the info value of the Arc.*/
        public int getInfo() {
            return info;
        }

        public void setNext(Arc a) {
            next = a;
        }
    }

    /**
     * Graph represents a collection of vertexes and arcs.
     */
    class Graph {

        private String id;
        private Vertex first;
        private Vertex enter;
        private Vertex exit;
        private int info = 0;
        private List<Arc> bestPath = new ArrayList<>();
        private int bestPathValue = 0;
        private ArrayList<ArrayList<Arc>> allRoutes = new ArrayList<>();

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        public List<Arc> getProfitPath() {
            getPathToEnd(this.enter, new ArrayList<Arc>());
            if (this.bestPath.isEmpty()) {
                return new ArrayList<>();
            }
            return this.bestPath;
        }

        public void setEnter(Vertex enter) {
            enter.setEnter(true);
            this.enter = enter;
        }

        public void setExit(Vertex exit) {
            exit.setExit(true);
            this.exit = exit;
        }

        /**
         * Method for finding the most valuable path to EXIT vertex using recursion.
         *
         * @param vertex is the vertex we check if is EXIT else moving to the next vertex through the most valuable
         *               arc not yet in the used path.
         * @param path   is a list of arcs we cannot use anymore.
         */
        private void getPathToEnd(Vertex vertex, List<Arc> path) {
            if (vertex.exit) {
                int pathValue = getPathValue(path);
                if (this.bestPathValue < pathValue) {
                    this.bestPathValue = pathValue;
                    this.bestPath = new ArrayList<>(path);
                }
            } else {
                List<Arc> usedArcs = new ArrayList<>();
                Arc arc = getMostValuableArc(vertex, path);
                while (arc != null) {
                    path.add(arc);
                    getPathToEnd(arc.target, path);
                    path.remove(path.get(path.size() - 1));
                    usedArcs.add(arc);
                    arc = getMostValuableArc(vertex, addPathsTogether(path, usedArcs));
                }
            }
        }

        /**
         * Method that adds two arc lists into one.
         *
         * @param pathOne is the first list.
         * @param pathTwo is the second list.
         * @return a new list of arcs.
         */
        private List<Arc> addPathsTogether(List<Arc> pathOne, List<Arc> pathTwo) {
            List<Arc> result = new ArrayList<>();
            result.addAll(pathOne);
            result.addAll(pathTwo);
            return result;
        }

        /**
         * Method that gives a string version of the most valuable path.
         *
         * @return is a string of the most valuable path.
         */
        private String pathToString() {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < bestPath.size(); i++) {
                if (i == bestPath.size() - 1) {
                    result.append(bestPath.get(i).from.id)
                            .append(" -(")
                            .append(bestPath.get(i).value)
                            .append(")-> ")
                            .append(bestPath.get(i).target.id)
                            .append(", with the total value of ")
                            .append(bestPathValue)
                            .append(" gold coins!");
                } else {
                    result.append(bestPath.get(i).from.id)
                            .append(" -(")
                            .append(bestPath.get(i).value)
                            .append(")-> ");
                }
            }
            return result.toString();
        }

        /**
         * Method that returns the value of a given list of arcs.
         *
         * @param path a list of arcs.
         * @return is a sum of arc values in the list of arcs.
         */
        private int getPathValue(List<Arc> path) {
            int result = 0;
            for (Arc arc : path) {
                result += arc.value;
            }
            return result;
        }

        /**
         * Method that returns the most valuable arc of a vertex that is not in the list of arcs.
         *
         * @param vertex   is the vertex which arc we are trying to find.
         * @param usedArcs is a list of arcs that cannot be used.
         * @return is the most valuable arc that can be used.
         */
        public Arc getMostValuableArc(Vertex vertex, List<Arc> usedArcs) {
            Arc result = null;
            Arc arc = vertex.first;
            if (!usedArcs.contains(arc)) {
                result = arc;
            }
            while (arc != null) {
                if (!usedArcs.contains(arc)) {
                    if (arc.next != null) {
                        if (arc.value < arc.next.value) {
                            if (!usedArcs.contains(arc.next)) {
                                result = arc.next;
                            }
                        }
                    } else if (result != null) {
                        if (arc.value > result.value) {
                            result = arc;
                        }
                    } else {
                        result = arc;
                    }
                }
                arc = arc.next;
            }
            return result;
        }

        /**
         * Method for finding the furthest vertex from the first vertex. It can be used only for directed acyclic graph,
         * which has only one start vertex and only one ending vertex.
         * @param first first vertex of the graph
         * @return the last vertex/destination vertex of the graph
         */
        public Vertex getDestinationVertex(Vertex first) {
            while (first.next != null) {
                first = first.next;
            } return first;
        }

        /**
         * Helper method for searching for all possible simple paths between two vertices. Method uses Depth-first Search
         * with recursion.
         * First idea was from https://www.baeldung.com/cs/simple-paths-between-two-vertices but I wrote the code by
         * myself.
         * @param source the source Vertex
         * @param destination the destination Vertex
         * @param route empty ArrayList of Arcs
         */
        public void getAllRoutes(Vertex source, Vertex destination, ArrayList<Arc> route) {
            if (source.equals(destination)) {
                allRoutes.add((ArrayList<Arc>) route.clone()); // route is first cloned and then added to list of routes
            }
            if (source.first != null) { // looking for arcs from the vertices
                Arc a = source.first;
                if (!route.contains(a)) {
                    route.add(a); // arc is added to the route
                    getAllRoutes(a.target, destination, route); // route is scanned from next vertex to destination recursively
                    int index = route.size() - 1;
                    route.remove(index);
                }
                while (a.next != null) { // looking for another arcs from current vertex
                    a = a.next;
                    if (!route.contains(a)) {
                        route.add(a); // arc is added to the route
                        getAllRoutes(a.target, destination, route); // route is scanned from next vertex to destination recursively
                        int index = route.size() - 1;
                        route.remove(index);
                    }
                }
            }
        }

        /**
         * Method for finding all routes, which satisfy the number of dangerous slopes (red arcs) desired by the
         * client/skier.
         * @param redArcLimit integer of the number of dangerous slopes
         * @return ArrayList of ArrayList of Arcs which is list of routes from top of the mountain. Each element of the
         *         list is also a list of arcs.
         */
        public String getClientRoutes(int redArcLimit) {
            ArrayList<ArrayList<Arc>> result = new ArrayList<>();
            for (ArrayList<Arc> r: allRoutes) {
                int redArcs = 0;
                for (Arc a: r) {
                    if (a.isRedLine) {
                        redArcs++;
                    }
                }
                if (redArcs == redArcLimit) { // finding if the route has as many dangerous/red slopes as client wants
                    result.add(r);
                }
            }
            if (result.isEmpty()) { // if there are no route with entered red arcs, the exception is thrown
                return "Tulemus: Puudub " + redArcLimit + " ohtliku laskumisega võimalus.";
            }
            StringBuilder sb = new StringBuilder();
            for (ArrayList<Arc> r: result) {
                if (sb.length() == 0) {
                    sb.append(r);
                } else {
                    sb.append("\n                                             ");
                    sb.append(r);
                }
            }
            return "Tulemus: " + redArcLimit + " ohtliku laskumisega võimalused on " + sb;
        }

        /**
         * Convert existing directed graph to line graph.
         *
         * @return line graph
         */
        public Graph convertToLineGraph() {
            ArrayList<Vertex> newVertexList = getLineGraphVertexes();
            connectVertexes(createAdjMatrix(), newVertexList);
            Graph temp = new Graph("L(" + this.id.replaceAll("\\s+", "") + ")", newVertexList.get(0));
            Vertex vertex = temp.first;
            while (vertex != null){
                if (vertex.enter){
                    temp.setEnter(vertex);
                } else if (vertex.exit){
                    temp.setExit(vertex);
                }
                vertex = vertex.next;
            }
            return temp;
        }

        /**
         * Create list of line graph's vertexes from original graph's arcs.
         * Add origin vertex's index into info field.
         * @return an arraylist of vertexes
         */
        private ArrayList<Vertex> getLineGraphVertexes() {
            ArrayList<Vertex> res = new ArrayList<>();
            Vertex last = null;
            Vertex v = first;
            boolean enter = true;
            boolean exit = true;
            int vertexIndex = 0;
            while (v != null) {
                Arc a = v.first;
                while (a != null) {
                    Vertex newVer = new Vertex(a.id.replaceAll("\\s+", ""));
                    newVer.info = vertexIndex;
                    if (v.enter && enter){
                        newVer.setEnter(true);
                        enter = false;

                    }
                    if (last != null) {
                        last.next = newVer;
                    }
                    res.add(newVer);
                    a = a.next;
                    last = newVer;
                }
                if (v.exit && exit){
                    res.get(res.size() - 1).setExit(true);
                    exit = false;
                }
                v = v.next;
                vertexIndex++;
            }
            return res;
        }

        /**
         * Connect line graph's vertexes based on original graph's adjacency matrix.
         * Using vertexes' info field to find arc's target.
         * @param adjMatrix original graph's adjacency matrix
         * @param vertexList an arraylist of line graph's vertexes
         */
        private void connectVertexes(int[][] adjMatrix, ArrayList<Vertex> vertexList) {
            int fromVerIndex = 0;
            for (int[] row : adjMatrix) {
                for (int i = 0; i < row.length; i++) {
                    Arc last = null;
                    if (row[i] == 1) {
                        int k = 0;     // index of the arc from the same vertex
                        for (int j = 0; j < adjMatrix[i].length; j++) {
                            if (adjMatrix[i][j] == 1) {
                                Vertex target = findTarget(vertexList, i, k);
                                int valueOne = (int) (Math.random() * 10);
                                Arc newArc = new Arc("a" + vertexList.get(fromVerIndex).id + "_" + target.id);
                                newArc.target = target;
                                newArc.from = vertexList.get(fromVerIndex);
                                newArc.value = valueOne;
                                if (vertexList.get(fromVerIndex).first == null) {
                                    vertexList.get(fromVerIndex).first = newArc;
                                }
                                if (last != null) {
                                    last.next = newArc;
                                }
                                last = newArc;
                                k++;
                            }
                        }
                        fromVerIndex++;
                    }
                }
            }
        }

        /**
         * Find and return a vertex form list using info field original vertex's index and arc's index.
         * @param vertexList list of vertexes
         * @param i original vertex's index
         * @param j vertex's arc's index
         * @return target vertex
         */
        private Vertex findTarget(ArrayList<Vertex> vertexList, int i, int j) {
            int k = 0;
            for (Vertex v: vertexList) {
                if (v.info == i) {
                    if (j == k) {
                        return v;
                    }
                    k++;
                }
            }
            throw new RuntimeException("Could not find target vertex.");
        }

        /**
         * Count all vertexes and arcs in this graph.
         * @return array of vertex count and arc count
         */
        public int[] getVertexAndArcCount() {
            int verCount = 0;
            int arcCount = 0;
            if (first != null) {
                Vertex v = first;
                while (v != null) {
                    verCount++;
                    Arc a = v.first;
                    while (a != null) {
                        arcCount++;
                        a = a.next;
                    }
                    v = v.next;
                }
            }
            return new int[]{verCount, arcCount};
        }

        /**
         * Task: Multiply the graph with it self. Graph to the power of 2
         * @return returns graph which is to the power of 2
         */
        public Graph graphToThePowerOf2 (){
            Graph graph = new Graph("Graph");

            Vertex v = first;

            while (v != null){
                Vertex vertex = graph.createVertex(v.id);
                Arc arc = v.first;

                while (arc != null){
                    Arc arc2 = arc.target.first;

                    while (arc2 != null){
                        if (arc2.target != v){
                            Vertex vertex1 = arc2.target;
                            graph.createArc("a" + arc.from.toString() + "_" + arc2.target.toString(), vertex, vertex1, arc2.value + arc.value);

                        }
                        arc2 = arc2.next;

                    }
                    arc = arc.next;
                }
                v = v.next;
            }


            return graph;
        }

        /**
         * Delete a Vertex and its predecessors (parents) from the Graph.
         * Two conditions must be met before deleting:
         * 1) input graph ('this') must be valid,
         * 2) vertex must exist in the graph ('this')
         * Otherwise an exception is thrown to the user.
         *
         * @param vertexToDelete Vertex to be 'deleted' from the graph
         */

        public void deleteVertexAndItsPredecessors(Vertex vertexToDelete) {

            if (first == null) {
                throw new IllegalArgumentException("\n" + "Can't delete a vertex from an empty graph! " + "\n" +
                        "Please restart and try again.");
            }

            try {
                markGraphElementsToBeDeleted(vertexToDelete);
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException("\n" + "Vertex requested to be deleted does not exist in the graph!" +
                        "\n" + "Please restart and try again.");
            }

            // SET NEW firstVertex, IF NECESSARY
            setNewFirstVertex();

            // DELETE PREDECESSOR VERTEXES
            deleteVertexes();

            // DELETE INVALID ARCS FOR EACH LEFTOVER VERTEX
            deleteInvalidArcs();
        }

        /**
         * Method marks all the Vertexes and Arcs related to the parameter vertexToDelete.
         * Info values are set according to the following scheme:
         * 1 -- Vertex requested to be deleted by the user
         * 2 -- Arc pointing to the Vertex requested to be deleted
         * 3 -- Parent/predecessor Vertex for the Vertex requested to be deleted
         * 4 -- Arc pointing to the parent/predecessor Vertex
         *
         * Method also checks if the Vertex requested to be deleted even exists in the graph and throws an
         * exception, if not found.
         *
         * @param vertexToDelete vertex to be deleted from the graph
         */
        private void markGraphElementsToBeDeleted(Vertex vertexToDelete) {
            boolean found = false;
            Vertex vertex1 = first;
            while (vertex1 != null) {
                if (vertex1.getVertexId().equals(vertexToDelete.getVertexId())) {
                    vertex1.setVInfo(1);
                    found = true;
                }
                Arc arc = vertex1.first;
                while (arc != null) {

                    if (arc.target.getVertexId().equals(vertexToDelete.getVertexId())) {
                        arc.target.setVInfo(1);
                        arc.setInfo(2);
                    }
                    if (arc.getInfo() == 2 && !vertex1.getVertexId().equals(vertexToDelete.getVertexId())) {
                        vertex1.setVInfo(3);
                    }

                    arc = arc.next;
                }
                vertex1 = vertex1.next;
            }
            if (!found) {
                throw new IllegalArgumentException();
            }

            Vertex vertex2 = first;
            while (vertex2 != null) {
                Arc a = vertex2.first;
                while (a != null) {
                    if (a.target.getCustomInfo() == 3) {
                        a.setInfo(4);
                    }
                    a = a.next;
                }
                vertex2 = vertex2.next;
            }
        }

        /**
         * Method sets a new firstVertex (an entry point for the graph) if the current firstVertex was marked as
         * a Vertex to be deleted (info value 1 or 3).
         * If a new firstVertex is required but was impossible to assign, method returns and the graph will eventually
         * become empty.
         */
        public void setNewFirstVertex() {
            int counter = 0; // COUNTS HOW MANY VERTEXES HAVE BEEN INSPECTED
            Vertex vertex = first;
            while (vertex != null) {
                if (vertex.getCustomInfo() == 3 && counter == 0 || vertex.getCustomInfo() == 1 && counter == 0) {
                    // IF FIRST VERTEX NEEDS TO BE DELETED, SET A NEW firstVertex
                    if (vertex.next != null) {
                        Vertex currentVertex = vertex.next;
                        if (currentVertex.getCustomInfo() == 1 || currentVertex.getCustomInfo() == 3) {
                            while (currentVertex.getCustomInfo() == 1 || currentVertex.getCustomInfo() == 3) {
                                // LOOK FOR THE NEXT FIRST VERTEX THAT DOES NOT REQUIRE TO BE DELETED
                                if (currentVertex.next != null) {
                                    currentVertex = currentVertex.next;
                                } else { // firstVertex CAN'T BE SET TO NO OTHER VERTEX
                                    first = null; // DELETE CURRENT VERTEX
                                    break;
                                }
                            }
                            if (first == null) {  // GRAPH WILL BECOME EMPTY
                                return;
                            }
                        }
                        first = currentVertex; // SET NEW firstVertex

                    } else { // firstVertex CAN'T BE SET TO NO OTHER VERTEX -> GRAPH WILL BECOME EMPTY
                        first = null;
                        return;
                    }
                }
                counter++;
                vertex = vertex.next;
            }
        }

        /**
         * Removes both the Vertex to be deleted and all its predecessors / parents of the vertex to be deleted by the user.
         * New 'nextVertex' values are set to remove / "forget" parent vertexes.
         * By doing that, all arcs exiting these parent vertexes (marked with info value '2') are also "forgotten".
         */
        public void deleteVertexes() {
            Vertex vertex = first;
            while (vertex != null) {
                if (vertex.next != null) {
                    if (vertex.next.getCustomInfo() == 1 || vertex.next.getCustomInfo() == 3) {
                        // IF nextVertex NEEDS TO BE DELETED
                        if (vertex.next.next != null) {
                            Vertex currentVertex = vertex.next.next;
                            if (currentVertex.getCustomInfo() == 1 || currentVertex.getCustomInfo() == 3) {
                                while (currentVertex.getCustomInfo() == 1 || currentVertex.getCustomInfo() == 3) {
                                    // LOOK FOR THE NEXT VERTEX THAT DOES NOT REQUIRE TO BE DELETED
                                    if (currentVertex.next != null) {
                                        currentVertex = currentVertex.next;
                                    } else {                      // nextVertex CAN'T BE SET TO NO OTHER VERTEX
                                        vertex.next = null;
                                        break;
                                    }
                                }
                            }
                            if (vertex.next != null) {
                                vertex.next = currentVertex; // SET NEW nextVertex
                            }
                        } else {
                            vertex.next = null;  // nextVertex CAN'T BE SET TO NO OTHER VERTEX
                        }
                    }

                }
                vertex = vertex.next;
            }
        }

        /**
         * Deletes all arcs pointing to the parent/predecessor Vertexes that were just deleted (and now appear as 'null').
         * These arcs were previously marked with info value '4'.
         */
        public void deleteInvalidArcs() {
            Vertex vertex = first;
            while (vertex != null) {
                boolean arrayIsRequired = false;
                int arraySize = 0;
                Arc a1 = vertex.first;
                while (a1 != null) {       // COUNT HOW MANY ARCS EXIT CURRENT VERTEX
                    arraySize++;
                    a1 = a1.next;
                    arrayIsRequired = true;
                }
                if (arrayIsRequired) {        // IF NO ARCS EXIT VERTEX THEN THE FOLLOWING IS NOT NEEDED

                    Arc[] arcs = getArcArray(vertex, arraySize);

                    markArcsToBeDeleted(arcs);

                    setNewArcSequence(vertex, arcs);
                }

                vertex = vertex.next; // REPEAT FOR ALL VERTEXES
            }
        }

        /**
         * Gets all arcs from a vertex and collects them into an array.
         *
         * @param vertex the Vertex that all exiting Arcs are collected from
         * @param arraySize number of Arcs exiting this Vertex
         * @return array of arcs
         */
        public Arc[] getArcArray(Vertex vertex, int arraySize) {
            Arc[] arcs = new Arc[arraySize];
            int index = 0;
            Arc arc = vertex.first;
            while (arc != null) {
                arcs[index] = arc;
                index++;
                arc = arc.next;
            }
            return arcs;
        }

        /**
         * Marks all arcs in the array as 'null' that are required to be deleted (marked with info value '4').
         *
         * @param arcs array of arcs
         */
        public void markArcsToBeDeleted(Arc[] arcs) {
            for (int j = 0; j < arcs.length; j++) {
                if (arcs[j].getInfo() == 4) {
                    arcs[j] = null;
                }
            }
        }

        /**
         * Resets the arcs exiting the Vertex and assigns new arcs.
         * New 'firstArc' and new 'nextArcs' are set for the Vertex.
         *
         * @param vertex the Vertex to which a new sequence of arcs is assigned
         * @param arcs  array of arcs
         */
        public void setNewArcSequence(Vertex vertex, Arc[] arcs) {

            vertex.first = null; // RESET FIRST ARC FOR CURRENT VERTEX

            if (arcs.length == 1) {
                if (arcs[0] != null) {
                    vertex.first = arcs[0];
                }
            }

            Arc currentArc = null;
            if (arcs.length > 1) {
                for (Arc arc : arcs) {
                    if (arc != null) {
                        if (vertex.first == null) { // IF NEW firstArc HAS NOT BEEN SET YET
                            vertex.first = arc;
                            currentArc = vertex.first;
                        } else {
                            currentArc.setNext(arc); // SET nextArc
                            currentArc = currentArc.next;
                        }
                        currentArc.next = null; // RESET nextArc
                    }
                }
            }
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.id);
                    sb.append("->");
                    sb.append(a.target.id);
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to, int value) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            res.from = from;
            res.value = value;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                if (n - i == n) {
                    varray[i] = createVertex("v" + String.valueOf(n - i));
                    this.setExit(varray[i]);
                } else if (n - i == 1) {
                    varray[i] = createVertex("v" + String.valueOf(n - i));
                    this.setEnter(varray[i]);
                } else {
                    varray[i] = createVertex("v" + String.valueOf(n - i));
                }
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    int valueOne = (int) (Math.random() * 4);
                    int valueTwo = (int) (Math.random() * 100);
                    Arc arc = createArc("a" + varray[i].id + "_" + varray[vnr].id, varray[i], varray[vnr], valueTwo);
                    if (valueOne == 2){
                        arc.isRedLine = true;
                    }
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (directed, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomDirectedGraph(int n, int m) {
            validInput(n, m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                int valueOne = (int) (Math.random() * 100);
                int valueTwo = (int) (Math.random() * 100);
                createArc("a" + vj.id + "_" + vi.id, vj, vi, valueTwo);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        public void validInput(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
        }
    }
}